/*! \file *********************************************************************
For ASF libs compliance
*******************************************************************************/

#ifndef __ASSERT_H
#define __ASSERT_H

#undef assert
#define assert(test) ;

#endif  // __ASSERT_H
